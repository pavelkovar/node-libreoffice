FROM node:15-buster

COPY . /app
WORKDIR /app

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends libreoffice locales tini \
    fonts-crosextra-caladea fonts-crosextra-carlito fonts-dejavu-extra fonts-dejavu fonts-dejavu-core \
    fonts-droid-fallback fonts-liberation2 fonts-liberation fonts-linuxlibertine fonts-linuxlibertine \
    fonts-noto-core fonts-noto-mono fonts-noto-ui-core fonts-opensymbol fonts-sil-gentium fonts-sil-gentium-basic \
 && dpkg-reconfigure --frontend=noninteractive fontconfig-config \
 && rm -rf /var/lib/apt/lists/* \
 && sed -i -e 's/# cs_CZ.UTF-8 UTF-8/cs_CZ.UTF-8 UTF-8/' /etc/locale.gen \
 && dpkg-reconfigure --frontend=noninteractive locales

ENV LANG cs_CZ.UTF-8
ENV LC_ALL cs_CZ.UTF-8
ENV TZ Europe/Prague

ENTRYPOINT ["/usr/bin/tini", "--"]
